<?php

function thim_child_enqueue_styles() {
	if ( is_multisite() ) {
		wp_enqueue_style( 'thim-child-style', get_stylesheet_uri() );
	} else {
		wp_enqueue_style( 'thim-parent-style', get_template_directory_uri() . '/style.css' );
	}
}

add_action( 'wp_enqueue_scripts', 'thim_child_enqueue_styles', 1000 );


/* Custom stylesheet with no dependencies, enqueued in the header */
function ecotec_custom_styles() {
	// Register my custom stylesheet
	wp_register_style('ecotec-style', get_stylesheet_directory_uri().'/style-ecotec.css', false, '1.0', 'all');
	// Load my custom stylesheet
	wp_enqueue_style('ecotec-style');
  }

add_action('wp_enqueue_scripts', 'ecotec_custom_styles',10001);



/* Custom script with no dependencies, enqueued in the header */
add_action('wp_enqueue_scripts', 'ecotec_custom_js', 10002);
function ecotec_custom_js() {
    wp_enqueue_script('ecotec-script', get_stylesheet_directory_uri().'/script-ecotec.js', false, '1.0', 'all');
}






/**
 * Change number of products that are displayed per page (shop page)
 */
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

function new_loop_shop_per_page( $cols ) {
  $cols = 20;
  return $cols;
}

function agrega_mime_type ( $mime_types ) {
	$mime_types['webp'] = 'image/webp';
	$mime_types['svg'] = 'image/svg+xml';
	return $mime_types;}
   
   add_filter('upload_mimes', 'agrega_mime_type', 1, 1);

